<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grid_view_test".
 *
 * @property integer $id
 * @property string $test_cell_1
 * @property integer $test_cell_2
 */
class GridViewTest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grid_view_test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_cell_1', 'test_cell_2'], 'required'],
            [['test_cell_2'], 'integer'],
            [['test_cell_1'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_cell_1' => 'Test Cell 1',
            'test_cell_2' => 'Test Cell 2',
        ];
    }
}
