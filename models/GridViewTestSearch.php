<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GridViewTestSearch represents the model behind the search form about `app\models\GridViewTest`.
 */
class GridViewTestSearch extends GridViewTest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'test_cell_2'], 'integer'],
            [['test_cell_1'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GridViewTest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'test_cell_2' => $this->test_cell_2,
        ]);

        $query->andFilterWhere(['like', 'test_cell_1', $this->test_cell_1]);

        return $dataProvider;
    }
}
