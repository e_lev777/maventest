<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\GridViewTestSearch[] */
/* @var $searchQueryParams \yii\web\Request[] | null */
/* @var $dataProvider \app\models\GridViewTest[] */

use yii\helpers\Html;
use app\widgets\Grid\Grid;
use app\widgets\Grid\core\TextColumn;
use app\widgets\Grid\core\ActionColumn;
use app\widgets\Grid\core\ArrayDataProvider;
use app\widgets\Grid\core\FilteredDataProvider;

$this->title = 'My Grid';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $Data = new ArrayDataProvider($dataProvider) ?>
    <?php $FilterData = new FilteredDataProvider($searchModel, $searchQueryParams) ?>

    <?= Grid::create($Data, $FilterData)
        ->showHeader(true)
        ->showFilter(true)
        ->withColumns([
                TextColumn::create()->forAttribute('id')->withLabel('ID'),
                TextColumn::create()->forAttribute('test_cell_1')->withLabel('Test Cell 1'),
                TextColumn::create()->forAttribute('test_cell_2')->withLabel('Test Cell 2'),
        ])
        ->withActionsColumn(ActionColumn::create())
        ->render();
    ?>

</div>
<script>
    $(function () {
        $('.my-grid-input').on('focusout', function () {
            var filter_values = $('.my-grid-input');
            var filters = MyGrid.listenFilters(filter_values);
            MyGrid.filterData(filters);
        });
    });

    var MyGrid = {};

    MyGrid.listenFilters = function (filter_values) {
        var filters = [];

        filter_values.each(function () {
            if ($(this).val() !== '') {
                var attribute = $(this).attr('name');
                var value = $(this).val();

                filters.push({name: attribute, value: value});
            }
        });
        return filters;
    };

    MyGrid.filterData = function (filters) {
        var location = window.location.pathname;

        if (filters.length > 0) {
            $(filters).each(function (key, element) {
                var query_param = key == 0 ? '?' : '&';
                location += query_param + element.name + '=' + element.value;
            });
        }
        window.location.assign(location);
    };
</script>
