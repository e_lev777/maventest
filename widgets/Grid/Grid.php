<?php

namespace app\widgets\Grid;

use app\widgets\Grid\interfaces\Filter;
use app\widgets\Grid\interfaces\Column;
use app\widgets\Grid\interfaces\Data;

class Grid
{
    private $header = true;
    private $filter = true;
    private $actions_column;
    private $columns = [];
    private $data;
    private $filter_data;

    /**
     * Grid constructor.
     * @param Data $Data
     * @param Filter|null $FilterData
     */
    public function __construct(Data $Data, Filter $FilterData = null) {
        $this->data = $Data;
        $this->filter_data = $FilterData;
    }

    /**
     * @param Data $Data
     * @param Filter|null $FilterData
     * @return static
     */
    public static function create(Data $Data, Filter $FilterData = null) {
        return new static($Data, $FilterData);
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function showHeader($value = true) {
        $this->header = $value;
        return $this;
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function showFilter($value = true) {
        $this->filter = $value;
        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function withColumns(array $columns) {
        $this->columns = $columns;
        return $this;
    }

    /**
     * @param Column $column
     * @return $this
     */
    public function withActionsColumn(Column $column) {
        $this->actions_column = $column;
        return $this;
    }

    /**
     * @return mixed
     */
    private function gridHasFilteredData() {
        return $this->filter_data->getSource();
    }

    /**
     * @return string
     */
    private function renderActionsHeader() {
        $str = '';
        if ($this->actions_column != null) {
            $str = '<td>'. $this->actions_column->getLabel() .'</td>';
        }

        return $str;
    }

    /**
     * @return string
     */
    private function renderActionsBody() {
        $str = '';
        if ($this->actions_column != null) {
            $actions = $this->actions_column->getActions();
            $str = '<td>';
            foreach ($actions as $key => $action) {
                $str .= '<a href="#">'. $action .'</a>';
                if ($key != count($actions) - 1) {
                    $str .= ' | ';
                }
            }
            $str .= '</td>';
        }

        return $str;
    }

    /**
     * @return string
     */
    private function renderHeader() {
        $str = '';
        if ($this->header == true) {
            $str .= '<thead><tr>';
            foreach ($this->columns as $column) {
                $str .= '<td>'. $column->getLabel() .'</td>';
            }
            $str .= $this->renderActionsHeader();
            $str .= '</tr></thead>';
        }

        return $str;
    }

    /**
     * @return string
     */
    private function renderBody() {
        $data = $this->gridHasFilteredData() ?
            $this->filter_data->getSource() :
            $this->data->getSource();

        $str = '<tbody>';

        foreach ($data as $item) {
            $str .= '<tr>';
            foreach ($this->columns as $column) {
                $attribute = $column->getAttribute();
                $str .= '<td>'. $item->$attribute .'</td>';
            }
            $str .= $this->renderActionsBody();
            $str .= '</tr>';
        }
        $str .= '</tbody>';

        return $str;
    }

    /**
     * @return string
     */
    private function renderFilter() {
        $str = '';

        if ($this->filter == true) {
            $str .= '<tr>';
            foreach ($this->columns as $column) {
                $attribute = $column->getAttribute();
                $str .= '<td>'.'<input class="my-grid-input" style="width: 100%" type="text" id="my_Grid_'.$column->getAttribute().'" name="'.$column->getAttribute().'" value="'.$this->filter_data->getQueryParam($attribute).'">'.'</td>';
            }
            $str .= $this->actions_column != null ? '<td></td>' : null;
            $str .= '</tr>';
        }

        return $str;
    }

    /**
     * @return string
     */
    public function render() {

        $table = '<table class="table table-striped table-bordered">';
        $table .= $this->renderHeader();
        $table .= $this->renderFilter();
        $table .= $this->renderBody();
        $table .= '</table>';

        return $table;
    }
}