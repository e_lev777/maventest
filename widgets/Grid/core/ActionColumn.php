<?php

namespace app\widgets\Grid\core;

use app\widgets\Grid\interfaces\Column;

class ActionColumn implements Column
{
    private $attribute = 'actions';
    private $label = 'Actions';
    private $actions = ['Create', 'Update', 'Delete'];

    /**
     * @return static
     */
    public static function create()
    {
        return new static;
    }

    /**
     * @param $attribute
     * @return $this
     */
    public function forAttribute($attribute) {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * @param $label
     * @return $this
     */
    public function withLabel($label) {
        $this->label = $label;
        return $this;
    }

    /**
     * @param array $actions
     * @return $this
     */
    public function withActions(array $actions) {
        $this->actions = $actions;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return array
     */
    public function getActions() {
        return $this->actions;
    }

}