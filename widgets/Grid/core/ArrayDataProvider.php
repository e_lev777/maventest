<?php

namespace app\widgets\Grid\core;

use app\widgets\Grid\interfaces\Data;

class ArrayDataProvider implements Data
{
    private $source;

    /**
     * ArrayDataProvider constructor.
     * @param array $Data
     */
    public function __construct(array $Data) {
        $this->source = $Data;
    }

    /**
     * @return array
     */
    public function getSource() {
        return $this->source;
    }
}