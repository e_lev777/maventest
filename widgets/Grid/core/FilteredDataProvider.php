<?php

namespace app\widgets\Grid\core;

use app\widgets\Grid\interfaces\Filter;

class FilteredDataProvider implements Filter
{
    private $query_params;
    private $source;

    /**
     * FilteredDataProvider constructor.
     * @param array|null $Data
     * @param array|null $QueryParams
     */
    public function __construct(array $Data = null, array $QueryParams = null) {
        $this->source = $Data;
        $this->query_params = $QueryParams;
    }

    /**
     * @return array
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function getQueryParam($key) {
        return isset($this->query_params[$key]) ? $this->query_params[$key] : null;
    }
}