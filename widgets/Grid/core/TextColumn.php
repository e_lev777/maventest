<?php

namespace app\widgets\Grid\core;

use app\widgets\Grid\interfaces\Column;

class TextColumn implements Column
{
    private $attribute;
    private $label;

    /**
     * @return static
     */
    public static function create() {
        return new static;
    }

    /**
     * @param $attribute
     * @return $this
     */
    public function forAttribute($attribute) {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * @param $label
     * @return $this
     */
    public function withLabel($label) {
        $this->label = $label;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttribute() {
        return $this->attribute;
    }

    /**
     * @return mixed
     */
    public function getLabel() {
        return $this->label;
    }
}