<?php

namespace app\widgets\Grid\core;

use yii\data\ActiveDataProvider;
use app\widgets\Grid\interfaces\Data;

class YiiActiveDataProvider implements Data
{
    private $source;

    /**
     * YiiActiveDataProvider constructor.
     * @param ActiveDataProvider $Data
     */
    public function __construct(ActiveDataProvider $Data) {
        $this->source = $Data;
    }

    /**
     * @return array
     */
    public function getSource() {
        return $this->source->getModels();
    }
}