<?php

namespace app\widgets\Grid\interfaces;


interface Column
{
    /**
     * @param $attribute
     * @return mixed
     */
    public function forAttribute($attribute);

    /**
     * @param $label
     * @return mixed
     */
    public function withLabel($label);

    /**
     * @return mixed
     */
    public function getAttribute();

    /**
     * @return mixed
     */
    public function getLabel();
}