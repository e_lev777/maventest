<?php

namespace app\widgets\Grid\interfaces;


interface Data
{
    /**
     * @return mixed
     */
    public function getSource();
}