<?php

namespace app\widgets\Grid\interfaces;


interface Filter
{
    /**
     * @param $key
     * @return mixed
     */
    public function getQueryParam($key);
}